package com.florantexii.simpleapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Florante XII on 5/28/2016.
 */

public class model  {
    private String title;
    private String cat_id;
    private String tappcointitle;
    private String tappcoinprice;
    private String tappcoincurrency;
    private String phonecreditstitle;
    private String phonecreditsitemtitle;
    private String phonecreditsitemprice;
    private String phonecreditsitemcurrency;
    private String phonecreditscategoryid;


    public model(String title,  String cat_id,  String tappcointitle,  String tappcoinprice,  String tappcoincurrency,  String phonecreditstitle,String phonecreditsitemtitle,String phonecreditsitemprice,String phonecreditsitemcurrency,String phonecreditscategoryid) {
        this.title = title;
        this.cat_id = cat_id;
        this.tappcointitle = tappcointitle;
        this.tappcoinprice = tappcoinprice;
        this.tappcoincurrency = tappcoincurrency;
        this.phonecreditstitle = phonecreditstitle;
        this.phonecreditsitemtitle = phonecreditsitemtitle;
        this.phonecreditsitemprice = phonecreditsitemprice;
        this.phonecreditsitemcurrency = phonecreditsitemcurrency;
        this.phonecreditscategoryid = phonecreditscategoryid;
    }

    public model() {
    }


    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTappcointitle() {
        return tappcointitle;
    }

    public void setTappcointitle(String tappcointitle) {
        this.tappcointitle = tappcointitle;
    }

    public String getTappcoinprice() {
        return tappcoinprice;
    }

    public void setTappcoinprice(String tappcoinprice) {
        this.tappcoinprice = tappcoinprice;
    }

    public String getTappcoincurrency() {
        return tappcoincurrency;
    }

    public void setTappcoincurrency(String tappcoincurrency) {
        this.tappcoincurrency = tappcoincurrency;
    }

    public String getPhonecreditstitle() {
        return phonecreditstitle;
    }

    public void setPhonecreditstitle(String phonecreditstitle) {
        this.phonecreditstitle = phonecreditstitle;
    }

    public String getPhonecreditsitemtitle() {
        return phonecreditsitemtitle;
    }

    public void setPhonecreditsitemtitle(String phonecreditsitemtitle) {
        this.phonecreditsitemtitle = phonecreditsitemtitle;
    }


    public String getPhonecreditsitemprice() {
        return phonecreditsitemprice;
    }

    public void setPhonecreditsitemprice(String phonecreditsitemprice) {
        this.phonecreditsitemprice = phonecreditsitemprice;
    }

    public String getPhonecreditsitemcurrency() {
        return phonecreditsitemcurrency;
    }

    public void setPhonecreditsitemcurrency(String phonecreditsitemcurrency) {
        this.phonecreditsitemcurrency = phonecreditsitemcurrency;
    }


    public String getPhonecreditscategoryid() {
        return phonecreditscategoryid;
    }

    public void setPhonecreditscategoryid(String phonecreditscategoryid) {
        this.phonecreditscategoryid = phonecreditscategoryid;
    }


}

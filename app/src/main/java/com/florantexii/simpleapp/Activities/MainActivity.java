package com.florantexii.simpleapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.views.RVOnItemTouchListener;
import com.florantexii.simpleapp.model.model;
import com.florantexii.simpleapp.views.CategoriesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<model> categories;
    private CategoriesAdapter lifePlanAdapter;
    private Toolbar toolbar;

    ProgressDialog dia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        categories = new ArrayList<model>();

        final RecyclerView rv = initRecyclerView();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sample App");

        dia = new ProgressDialog(MainActivity.this, R.style.StyledDialog);

        categories = new ArrayList<model>();


        String url = "https://sandbox-market.tapp.fi/api/1/product-categories/";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        dia.setMessage("");
        dia.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                dia.dismiss();
                for (int i = 0; i < response.length(); i++) {
                    model lifePlan = new model();

                    try {
                        JSONObject lifePlanObj = (JSONObject) response.get(i);
                        lifePlan.setTitle(lifePlanObj.getString("category-localized-title"));
                        lifePlan.setCat_id(lifePlanObj.getString("product-category-id"));

                        categories.add(lifePlan);

                    } catch (JSONException e) {
                        Log.d("JSON ERROR", e.toString());
                    }
                }
                lifePlanAdapter = new CategoriesAdapter(getApplicationContext(), categories);
                rv.setAdapter(lifePlanAdapter);
                rv.addOnItemTouchListener(new RVOnItemTouchListener(getApplicationContext(), new RVOnItemTouchListener.OnItemTouchListener() {
                    @Override
                    public void onItemTouch(View v, int position) {
                        if(position==0) {
                            Intent intent = new Intent(MainActivity.this, TappCoinActivity.class);
                            intent.putExtra("cat_id", categories.get(position).getCat_id());
                            intent.putExtra("cat_title", categories.get(position).getTitle());
                            //Toast.makeText(MainActivity.this, categories.get(position).getCat_id(), Toast.LENGTH_SHORT).show();
                            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this, R.anim.slide_in_left, R.anim.no_animation).toBundle();
                            startActivity(intent, bundle);
                        }
                        else if(position==1) {
                            Intent intent = new Intent(MainActivity.this, PhoneCreditsActivity.class);
                            intent.putExtra("cat_id", categories.get(position).getCat_id());
                            intent.putExtra("cat_title", categories.get(position).getTitle());
                            //Toast.makeText(MainActivity.this, categories.get(position).getCat_id(), Toast.LENGTH_SHORT).show();
                            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this, R.anim.slide_in_left, R.anim.no_animation).toBundle();
                            startActivity(intent, bundle);
                        }
                        else if(position==2) {
                            Intent intent = new Intent(MainActivity.this, TuitionsActivity.class);
                            intent.putExtra("cat_id", categories.get(position).getCat_id());
                            intent.putExtra("cat_title", categories.get(position).getTitle());
                            //Toast.makeText(MainActivity.this, categories.get(position).getCat_id(), Toast.LENGTH_SHORT).show();
                            Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this, R.anim.slide_in_left, R.anim.no_animation).toBundle();
                            startActivity(intent, bundle);
                        }
                    }
                }));
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do something
            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    @NonNull
    private RecyclerView initRecyclerView() {
        final RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(layoutManager);
        rv.setNestedScrollingEnabled(false);
        rv.setHasFixedSize(false);
        return rv;
    }
}

package com.florantexii.simpleapp.Activities;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;
import com.florantexii.simpleapp.views.TappCoinAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TappCoinActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private static String catid;
    private ArrayList<model> tappCoin;
    private TappCoinAdapter tappCoinAdapter;
    ProgressDialog dia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tappcoin);
        tappCoin = new ArrayList<model>();
        final RecyclerView rv = initRecyclerView();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("cat_title"));
        catid = getIntent().getExtras().getString("cat_id");

        dia = new ProgressDialog(TappCoinActivity.this, R.style.StyledDialog);

        //tappCoin = new ArrayList<model>();

        String url = "https://sandbox-market.tapp.fi/api/1/product-categories/"+catid;
       // Toast.makeText(TappCoinActivity.this,url,Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        dia.setMessage("");
        dia.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
               // Log.d(TAG, response.toString());

                try {


                    JSONArray items = response.getJSONArray("items");


                    for(int i=0; i < items.length(); i++)
                    {
                        model tappcoin = new model();
                        JSONObject types = items.getJSONObject(i);

                        JSONObject product = types.getJSONObject("product");

                        tappcoin.setTappcoinprice("Price: " + product.getString("price"));
                        tappcoin.setTappcointitle(product.getString("localized-description"));
                        tappcoin.setTappcoincurrency("Currency: " +product.getString("currency"));

                        tappCoin.add(tappcoin);
                    }

                    tappCoinAdapter = new TappCoinAdapter(getApplicationContext(), tappCoin);
                    rv.setAdapter(tappCoinAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                dia.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                dia.dismiss();
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_out_left);
    }

    @NonNull
    private RecyclerView initRecyclerView() {
        final RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(layoutManager);
        rv.setNestedScrollingEnabled(false);
        rv.setHasFixedSize(false);
        return rv;
    }
}

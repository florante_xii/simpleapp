package com.florantexii.simpleapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;
import com.florantexii.simpleapp.views.PhoneCreditsAdapter;
import com.florantexii.simpleapp.views.RVOnItemTouchListener;
import com.florantexii.simpleapp.views.TappCoinAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PhoneCreditsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private static String catid;
    private ArrayList<model> phoneCredits;
    private PhoneCreditsAdapter phoneCreditsAdapter;
    ProgressDialog dia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_credits);
        phoneCredits = new ArrayList<model>();
        final RecyclerView rv = initRecyclerView();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrowleft);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("cat_title"));
        catid = getIntent().getExtras().getString("cat_id");

        dia = new ProgressDialog(PhoneCreditsActivity.this, R.style.StyledDialog);

       // phoneCredits = new ArrayList<model>();

        String url = "https://sandbox-market.tapp.fi/api/1/product-categories/"+catid;
       // Toast.makeText(PhoneCreditsActivity.this, url, Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        dia.setMessage("");
        dia.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // Log.d(TAG, response.toString());

                try {

                    JSONArray items = response.getJSONArray("items");

                    for(int i=0; i < items.length(); i++)
                    {
                        model phonecreds = new model();
                        JSONObject types = (JSONObject) items.get(i);
                        //Toast.makeText(PhoneCreditsActivity.this,types.toString(),Toast.LENGTH_SHORT).show();
                        JSONObject category = types.getJSONObject("category");
                       // Toast.makeText(PhoneCreditsActivity.this,category.toString(),Toast.LENGTH_SHORT).show();
                        phonecreds.setPhonecreditstitle(category.getString("category-localized-title"));
                        //Toast.makeText(PhoneCreditsActivity.this,category.getString("category-localized-title"),Toast.LENGTH_SHORT).show();
                        phonecreds.setPhonecreditscategoryid(category.getString("product-category-id"));
                        phoneCredits.add(phonecreds);

                    }

                    phoneCreditsAdapter = new PhoneCreditsAdapter(getApplicationContext(), phoneCredits);
                    rv.setAdapter(phoneCreditsAdapter);
                    rv.addOnItemTouchListener(new RVOnItemTouchListener(getApplicationContext(), new RVOnItemTouchListener.OnItemTouchListener() {
                        @Override
                        public void onItemTouch(View v, int position) {
                                Intent intent = new Intent(PhoneCreditsActivity.this, PhoneCreditsItemActivity.class);
                                intent.putExtra("cat_id", phoneCredits.get(position).getPhonecreditscategoryid());
                                intent.putExtra("cat_title", phoneCredits.get(position).getPhonecreditstitle());
                                //Toast.makeText(MainActivity.this, categories.get(position).getCat_id(), Toast.LENGTH_SHORT).show();
                                Bundle bundle = ActivityOptionsCompat.makeCustomAnimation(PhoneCreditsActivity.this, R.anim.slide_in_left, R.anim.no_animation).toBundle();
                                startActivity(intent, bundle);

                        }
                    }));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                dia.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                dia.dismiss();
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_out_left);
    }

    @NonNull
    private RecyclerView initRecyclerView() {
        final RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(layoutManager);
        rv.setNestedScrollingEnabled(false);
        rv.setHasFixedSize(false);
        return rv;
    }
}

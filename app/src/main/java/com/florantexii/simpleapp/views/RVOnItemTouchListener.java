package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Florante XII on 5/28/2016.
 */
public class RVOnItemTouchListener implements RecyclerView.OnItemTouchListener {

    OnItemTouchListener listener;
    private final GestureDetector gestureDetector;
    public interface OnItemTouchListener{
        void onItemTouch(View v, int position);
    }

    public RVOnItemTouchListener(Context context, RVOnItemTouchListener.OnItemTouchListener listener) {
        this.gestureDetector = new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
        this.listener = listener;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(),e.getY());
        if ( null!= childView && null != listener && gestureDetector.onTouchEvent(e)){
            listener.onItemTouch(childView,rv.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}

package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;

import java.util.ArrayList;

/**
 * Created by Florante XII on 5/29/2016.
 */
public class TappCoinAdapter extends RecyclerView.Adapter<TappCoinAdapter.ViewHolder> {
    private Context context;
    private ArrayList<model> tappCoins;

    public TappCoinAdapter(Context context, ArrayList<model> tappCoins) {
        this.context = context;
        this.tappCoins = tappCoins;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tapp_coins_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tappcointitle.setText(tappCoins.get(position).getTappcointitle());
        holder.tappcoinprice.setText(tappCoins.get(position).getTappcoinprice());
        holder.tappcoincurrency.setText(tappCoins.get(position).getTappcoincurrency());

    }

    @Override
    public int getItemCount() {
        return tappCoins.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

        protected TextView tappcointitle;
        protected TextView tappcoinprice;
        protected TextView tappcoincurrency;
        public ViewHolder(View itemView) {

            super(itemView);
            this.tappcointitle = (TextView) itemView.findViewById(R.id.tappcointitle);
            this.tappcoinprice = (TextView) itemView.findViewById(R.id.tappcoinprice);
            this.tappcoincurrency = (TextView) itemView.findViewById(R.id.tappcoincurrency);
        }
    }
}
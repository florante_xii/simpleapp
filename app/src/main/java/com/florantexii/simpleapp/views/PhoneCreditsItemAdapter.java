package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;

import java.util.ArrayList;

/**
 * Created by Florante XII on 5/31/2016.
 */
public class PhoneCreditsItemAdapter extends RecyclerView.Adapter<PhoneCreditsItemAdapter.ViewHolder> {
    private Context context;
    private ArrayList<model> phoneCreditsItemList;

    public PhoneCreditsItemAdapter(Context context, ArrayList<model> phoneCreditsItemList) {
        this.context = context;
        this.phoneCreditsItemList = phoneCreditsItemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_credits_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.phonecreditsitemtitle.setText(phoneCreditsItemList.get(position).getPhonecreditsitemtitle());
        holder.phonecreditsitemprice.setText(phoneCreditsItemList.get(position).getPhonecreditsitemprice());
        holder.phonecreditsitemcurrency.setText(phoneCreditsItemList.get(position).getPhonecreditsitemcurrency());

    }

    @Override
    public int getItemCount() {
        return phoneCreditsItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView phonecreditsitemtitle;
        protected TextView phonecreditsitemprice;
        protected TextView phonecreditsitemcurrency;

        public ViewHolder(View itemView) {

            super(itemView);
            this.phonecreditsitemtitle = (TextView) itemView.findViewById(R.id.phonecreditsitemtitle);
            this.phonecreditsitemprice = (TextView) itemView.findViewById(R.id.phonecreditsitemprice);
            this.phonecreditsitemcurrency = (TextView) itemView.findViewById(R.id.phonecreditsitemcurrency);
        }
    }
}
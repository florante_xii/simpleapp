package com.florantexii.simpleapp.views;

/**
 * Created by Florante XII on 5/28/2016.
 */
public class config {
    public static final String DATA_URL = "https://sandbox-market.tapp.fi/api/1/product-categories/";
    public static final String product_category_id = "name";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_VC = "vc";
    public static final String JSON_ARRAY = "result";
}
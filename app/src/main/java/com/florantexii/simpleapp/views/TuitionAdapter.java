package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;

import java.util.ArrayList;

/**
 * Created by Florante XII on 5/30/2016.
 */
public class TuitionAdapter  extends RecyclerView.Adapter<TuitionAdapter.ViewHolder> {
    private Context context;
    private ArrayList<model> tuitionlist;

    public TuitionAdapter(Context context, ArrayList<model> tuitionlist) {
        this.context = context;
        this.tuitionlist = tuitionlist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_credits_category,parent,false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return tuitionlist.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

//        protected TextView phonecreditstitle;
//        protected ImageView logo;

        public ViewHolder(View itemView) {

            super(itemView);
//            this.phonecreditstitle = (TextView) itemView.findViewById(R.id.phonecreditstitle);
//            this.logo = (ImageView) itemView.findViewById(R.id.iv_logo);
        }
    }
}

package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;

import java.util.ArrayList;

/**
 * Created by Florante XII on 5/29/2016.
 */
public class PhoneCreditsAdapter extends RecyclerView.Adapter<PhoneCreditsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<model> phoneCredits;

    public PhoneCreditsAdapter(Context context, ArrayList<model> phoneCredits) {
        this.context = context;
        this.phoneCredits = phoneCredits;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_credits_category,parent,false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.phonecreditstitle.setText(phoneCredits.get(position).getPhonecreditstitle());
//        if(phoneCredits.get(position).getPhonecreditstitle().equals("ABSCBN")){
//            holder.logo.setImageResource(R.drawable.abs);
//        }else if (phoneCredits.get(position).getPhonecreditstitle().equals("GLOBE")){
//            holder.logo.setImageResource(R.drawable.globe);
//        }

    }

    @Override
    public int getItemCount() {
        return phoneCredits.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

        protected TextView phonecreditstitle;
        protected ImageView logo;

        public ViewHolder(View itemView) {

            super(itemView);
            this.phonecreditstitle = (TextView) itemView.findViewById(R.id.phonecreditstitle);
            this.logo = (ImageView) itemView.findViewById(R.id.iv_logo);
        }
    }
}

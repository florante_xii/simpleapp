package com.florantexii.simpleapp.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.florantexii.simpleapp.R;
import com.florantexii.simpleapp.model.model;

import java.util.ArrayList;

/**
 * Created by Florante XII on 5/28/2016.
 */



public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    private Context context;
    private ArrayList<model> categories;

    public CategoriesAdapter(Context context, ArrayList<model>  lifePlans) {
        this.context = context;
        this.categories = lifePlans;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cat_title.setText(categories.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{

        protected TextView cat_title;
        public ViewHolder(View itemView) {

            super(itemView);
            this.cat_title = (TextView) itemView.findViewById(R.id.cat_name);


        }
    }
}